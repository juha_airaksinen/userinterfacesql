-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for opsuor
CREATE DATABASE IF NOT EXISTS `opsuor` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `opsuor`;

-- Dumping structure for table opsuor.kurssi
CREATE TABLE IF NOT EXISTS `kurssi` (
  `kurssi_id` int(11) NOT NULL,
  `nimi` varchar(40) NOT NULL,
  `opintopisteet` int(11) NOT NULL,
  `kuvaus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`kurssi_id`),
  KEY `Kurssi_nimi_index` (`nimi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table opsuor.kurssi: ~3 rows (approximately)
/*!40000 ALTER TABLE `kurssi` DISABLE KEYS */;
INSERT IGNORE INTO `kurssi` (`kurssi_id`, `nimi`, `opintopisteet`, `kuvaus`) VALUES
	(1, 'SQL syntax', 3, 'Basic syntax of SQL'),
	(2, 'Programming Basics', 12, 'Focuses on Java and Objects'),
	(3, 'User Interfaces', 3, 'Basics of Javafx');
/*!40000 ALTER TABLE `kurssi` ENABLE KEYS */;

-- Dumping structure for table opsuor.opintosuoritus
CREATE TABLE IF NOT EXISTS `opintosuoritus` (
  `opiskelija_id` int(11) NOT NULL,
  `kurssi_id` int(11) NOT NULL,
  `arvosana` int(11) NOT NULL,
  `suoritus_pvm` datetime NOT NULL,
  PRIMARY KEY (`opiskelija_id`,`kurssi_id`),
  KEY `suoritus_opiskelija_id_index` (`opiskelija_id`),
  KEY `suoritus_kurssi_id_index` (`kurssi_id`),
  CONSTRAINT `opintosuoritus_ibfk_1` FOREIGN KEY (`kurssi_id`) REFERENCES `kurssi` (`kurssi_id`) ON DELETE CASCADE,
  CONSTRAINT `opintosuoritus_ibfk_2` FOREIGN KEY (`opiskelija_id`) REFERENCES `opiskelija` (`opiskelija_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table opsuor.opintosuoritus: ~1 rows (approximately)
/*!40000 ALTER TABLE `opintosuoritus` DISABLE KEYS */;
INSERT IGNORE INTO `opintosuoritus` (`opiskelija_id`, `kurssi_id`, `arvosana`, `suoritus_pvm`) VALUES
	(1, 3, 3, '2020-03-05 00:00:00'),
	(2, 2, 2, '2020-03-18 00:00:00');
/*!40000 ALTER TABLE `opintosuoritus` ENABLE KEYS */;

-- Dumping structure for table opsuor.opiskelija
CREATE TABLE IF NOT EXISTS `opiskelija` (
  `opiskelija_id` int(11) NOT NULL,
  `etunimi` varchar(20) DEFAULT NULL,
  `sukunimi` varchar(40) DEFAULT NULL,
  `lahiosoite` varchar(40) DEFAULT NULL,
  `postitoimipaikka` varchar(30) DEFAULT NULL,
  `postinro` varchar(5) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `puhelinnro` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`opiskelija_id`),
  KEY `Opiskelija_sukunimi_index` (`sukunimi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table opsuor.opiskelija: ~2 rows (approximately)
/*!40000 ALTER TABLE `opiskelija` DISABLE KEYS */;
INSERT IGNORE INTO `opiskelija` (`opiskelija_id`, `etunimi`, `sukunimi`, `lahiosoite`, `postitoimipaikka`, `postinro`, `email`, `puhelinnro`) VALUES
	(1, 'Joni', 'Liimainen', 'Lähiosoite 15', 'Lentoinen', '58882', 'Joni@gmail.com', '0504882223'),
	(2, 'Tomi', 'Louta', 'Harkatie 10', 'Liista', '88222', 'tomi@gmail.com', '0503323234');
/*!40000 ALTER TABLE `opiskelija` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
