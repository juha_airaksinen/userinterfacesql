import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Handles all the action events with the fxml file
 */
public class controller {

    @FXML
    private TextField completion_course_ID;
    @FXML
    private TextField completion_student_ID;
    private String [] grades = {"0", "1", "2", "3", "4", "5"};
    @FXML
    private ChoiceBox<String> completion_score;
    @FXML
    private DatePicker completion_date;

    @FXML
    private TextField course_ID;
    @FXML
    private TextField course_name;
    @FXML
    private TextField course_points;
    @FXML
    private TextArea course_desc;

    @FXML
    private TextField student_ID;
    @FXML
    private TextField student_firstName;
    @FXML
    private TextField student_lastName;
    @FXML
    private TextField student_address;
    @FXML
    private TextField student_postalNo;
    @FXML
    private TextField student_postalArea;
    @FXML
    private TextField student_email;
    @FXML
    private TextField student_phoneNo;

    @FXML
    private TextField completionSearch;
    @FXML
    private TextField courseSearch;
    @FXML
    private TextField studentSearch;

    @FXML
    private Label charCount;

    @FXML
    private TableView<courseCompletion> search_table;
    @FXML
    private TableColumn<courseCompletion, String> completion_column_courseID;
    @FXML
    private TableColumn<courseCompletion, String> completion_column_studentID;
    @FXML
    private TableColumn<courseCompletion, String> completion_column_grade;
    @FXML
    private TableColumn<courseCompletion, String> search_column_date;

    @FXML
    private TableView<course> course_table;
    @FXML
    private TableColumn<course, String> course_column_courseID;
    @FXML
    private TableColumn<course, String> course_column_name;
    @FXML
    private TableColumn<course, String> course_column_points;
    @FXML
    private TableColumn<course, String> course_column_description;

    @FXML
    private TableView<student> student_table;
    @FXML
    private TableColumn<student, String> student_column_studentID;
    @FXML
    private TableColumn<student, String> student_column_firstName;
    @FXML
    private TableColumn<student, String> student_column_lastName;
    @FXML
    private TableColumn<student, String> student_column_address;
    @FXML
    private TableColumn<student, String> student_column_postNum;
    @FXML
    private TableColumn<student, String> student_column_postArea;
    @FXML
    private TableColumn<student, String> student_column_email;
    @FXML
    private TableColumn<student, String> student_column_phoneNum;

    private boolean searchBool = true;

    databaseHandler dbHandler;

    /**
     * Uses the default databaseHandler with default addresses
     * change this to the correct one
     */
    public controller() {
        dbHandler = new databaseHandler();
    }

    /**
     * After startup defines what info the columns house
     */
    @FXML
    public void initialize() {

        completion_column_courseID.setCellValueFactory(new PropertyValueFactory<>("course_ID"));
        completion_column_studentID.setCellValueFactory(new PropertyValueFactory<>("student_ID"));
        completion_column_grade.setCellValueFactory(new PropertyValueFactory<>("grade"));
        search_column_date.setCellValueFactory(new PropertyValueFactory<>("completion_date"));

        course_column_courseID.setCellValueFactory(new PropertyValueFactory<>("course_ID"));
        course_column_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        course_column_points.setCellValueFactory(new PropertyValueFactory<>("points"));
        course_column_description.setCellValueFactory(new PropertyValueFactory<>("desc"));

        student_column_studentID.setCellValueFactory(new PropertyValueFactory<>("student_ID"));
        student_column_firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        student_column_lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        student_column_address.setCellValueFactory(new PropertyValueFactory<>("address"));
        student_column_phoneNum.setCellValueFactory(new PropertyValueFactory<>("phoneNum"));
        student_column_postNum.setCellValueFactory(new PropertyValueFactory<>("postNum"));
        student_column_postArea.setCellValueFactory(new PropertyValueFactory<>("postArea"));
        student_column_email.setCellValueFactory(new PropertyValueFactory<>("email"));

        completion_score.setItems(FXCollections.observableArrayList(grades));
    }

    /**
     * Informs the user of incorrect inputs
     *
     * @param e used for some issues to display where the incorrect input is
     */
    private void alertEvent(SQLException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("SQL Exception");
        switch (e.getErrorCode()) {
            case 0:    alert.setContentText("That ID doesn't exist check the table"); break;
            case 1062: alert.setContentText("Given ID or IDs already exist, cannot create duplicates"); break;
            case 1264: alert.setContentText("One or more fields was too long:\n" + e.getMessage()); break;
            case 1366: alert.setContentText("One or more of the required fields was empty"); break;
            case 1406: alert.setContentText("One of the fields was too long:\n" + e.getMessage()); break;
            case 1452: alert.setContentText("One of the IDs doesn't exist in other tables"); break;
            default:   alert.setContentText("SQL error code: " + e.getErrorCode() + ", message: " + e.getMessage());
                //generic error catch for more rare issues
        }
        alert.showAndWait();
    }

    /**
     * Used for notifying about null error if the user leaves the DateField
     * empty
     */
    private void alertEvent() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Empty field");
        alert.setContentText("Date field was empty\n");
        alert.showAndWait();
    }

    /**
     * Adds a completion the course and student needs to exist on the other
     * tables or it throws an error
     * everything is required
     */
    @FXML
    public void btnAddCompletion() {
        try {
            dbHandler.addCompletion(
                    completion_course_ID.getText(),
                    completion_student_ID.getText(),
                    completion_score.getValue(),
                    completion_date.getValue().toString());
        } catch (SQLException e) {
            alertEvent(e);
        } catch (Exception e) {
            alertEvent();
        }
    }

    /**
     * Fills a completion info, needs valid student and course IDs
     */
    @FXML
    public void btnFillCompletion() {
        try {
            ResultSet rs = dbHandler.getSpecificCompletion(completion_course_ID.getText(), completion_student_ID.getText());
            rs.next();
            completion_score.setValue(String.valueOf(rs.getInt("arvosana")));
            completion_date.setValue(rs.getDate("suoritus_pvm").toLocalDate());
        } catch (SQLException e) {
            alertEvent(e);
        } catch (Exception e) {
            alertEvent();
        }
    }

    /**
     * Changes a specific completion requires both student and course IDs
     */
    @FXML
    public void btnChangeCompletion() {
        try {
            dbHandler.updateCompletion(
                    completion_course_ID.getText(),
                    completion_student_ID.getText(),
                    completion_score.getValue(),
                    completion_date.getValue().toString());
        } catch (SQLException e) {
            alertEvent(e);
        } catch (Exception e) {
            alertEvent();
        }
    }

    /**
     * Removes a specific completion both student_ID
     * and course_ID need to be specified and match one in the databasse
     * asks user for confirmation
     */
    @FXML
    public void btnRemoveCompletion() {

        try {
            ResultSet rsStudent = dbHandler.getStudentDetails(completion_student_ID.getText());
            ResultSet rsCourse = dbHandler.getCourseDetails(completion_course_ID.getText());
            if (rsStudent != null && rsCourse != null) {
                rsStudent.next();
                rsCourse.next();
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setHeaderText("Poisto vahvistus");
                alert.setContentText("Haluatko poistaa suorituksen opiskelijalta: " +
                        rsStudent.getString("etunimi") + " " +
                        rsStudent.getString("sukunimi") + "\nKurssilta: " +
                        rsCourse.getString("nimi"));
                Optional<ButtonType> buttonResult = alert.showAndWait();
                if (buttonResult.isPresent()) {
                    if (buttonResult.get() == ButtonType.OK) {
                        dbHandler.removeCompletion(completion_course_ID.getText(), completion_student_ID.getText());
                    }
                }

            }
        } catch (SQLException e) {
            alertEvent(e);
        }
    }

    /**
     * Depending on the searchBool searches for the corresponding ID
     * if true searches for course_ID
     * if false searches for student_ID
     */
    @FXML
    public void btnSearchCompletion() {
        ResultSet rs;
        try {
            if (searchBool) {
                rs = dbHandler.getCourseSearch(completionSearch.getText());
            } else {
                rs = dbHandler.getStudentSearch(completionSearch.getText());
            }
            fillCompletionTable(rs);
        } catch (SQLException e) {
            alertEvent(e);
        }
    }

    /**
     * Fills the table with objects
     *
     * @param rs result set either from a student_ID search or course_ID search
     *           from completion table
     */
    private void fillCompletionTable(ResultSet rs) {
        search_table.getItems().clear();
        try {
            while (rs.next()) {
                search_table.getItems().add(new courseCompletion(rs.getInt("opiskelija_id"),
                        rs.getInt("kurssi_id"), rs.getInt("arvosana"),
                        rs.getDate("suoritus_pvm")));
            }
        } catch (SQLException e) {
            alertEvent(e);
        }
    }

    /**
     * Used to track search status
     * part of radiobutton which changes the search to course_ID
     */
    @FXML
    public void setSearchCourse() {
        searchBool = true;
    }

    /**
     * Used to track search status
     * part of a radiobutton which changes the search to student_ID
     */
    @FXML
    public void setSearchStudent() {
        searchBool = false;
    }

    /**
     * Adds a course to the database
     */
    @FXML
    public void btnAddCourse() {
        try {
            dbHandler.addCourse(course_ID.getText(), course_name.getText(), course_points.getText(), course_desc.getText());
        } catch (SQLException e) {
            alertEvent(e);
        }
    }

    /**
     * Fills TextFields with the corresponding info from the database for
     * quick editing
     */
    @FXML
    public void btnFillCourse() {
        try {
            ResultSet rs = dbHandler.getSpecificCourse(course_ID.getText());
            rs.next();
            course_name.setText(rs.getString("nimi"));
            course_points.setText(String.valueOf(rs.getInt("opintopisteet")));
            course_desc.setText(rs.getString("kuvaus"));
            updateCharCount();
        } catch (SQLException e) {
            alertEvent(e);
        }
    }

    /**
     * Changes a given IDs values
     */
    @FXML
    public void btnChangeCourse() {
        try {
            dbHandler.updateCourse(course_ID.getText(), course_name.getText(), course_points.getText(), course_desc.getText());
        } catch (SQLException e) {
            alertEvent(e);
        }
    }

    /**
     * Removes a specific course
     * asks for confirmation from the user
     */
    @FXML
    public void btnRemoveCourse() {
        try {
            ResultSet rs = dbHandler.getCourseDetails(course_ID.getText());
            if (rs != null) {
                rs.next();
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setHeaderText("Poisto vahvistus");
                alert.setContentText("Haluatko poistaa kurssin: " +
                        rs.getInt("kurssi_id") + " " +
                        rs.getString("nimi") + "\n" +
                        rs.getString("kuvaus"));
                Optional<ButtonType> buttonResult = alert.showAndWait();
                if (buttonResult.isPresent()) {
                    if (buttonResult.get() == ButtonType.OK) {
                        dbHandler.removeCourse(course_ID.getText());
                    }
                }
            }
        } catch (SQLException e) {
            alertEvent(e);
        }
    }

    /**
     * Used to fill the course table
     */
    @FXML
    public void btnSearchCourse() {

        /*
        clearing the table before filling it so it doesn't retain previous searches
         */
        course_table.getItems().clear();

        try {
            ResultSet rs = dbHandler.getCourseDetails(courseSearch.getText());
            while (rs.next()) {
                course_table.getItems().add(new course(
                        rs.getInt("kurssi_id"),
                        rs.getString("nimi"),
                        rs.getInt("opintopisteet"),
                        rs.getString("kuvaus")));
            }
        } catch (SQLException e) {
            alertEvent(e);
        }

    }

    /**
     * Description length updates with every key release and type
     */
    @FXML
    public void updateCharCount() {
        charCount.setText(course_desc.getText().length() + "/255");
    }

    /**
     * Adds a student
     * duplicates cannot be added
     */
    @FXML
    public void btnAddStudent() {
        try {
            dbHandler.addStudent(
                    student_ID.getText(),
                    student_firstName.getText(),
                    student_lastName.getText(),
                    student_address.getText(),
                    student_postalArea.getText(),
                    student_postalNo.getText(),
                    student_email.getText(),
                    student_phoneNo.getText());
        } catch (SQLException e) {
            alertEvent(e);
        }
    }

    /**
     * Used for filling all the TextFields for editing purposes
     */
    @FXML
    public void btnFillStudent() {
        try {
            ResultSet rs = dbHandler.getSpecificStudent(student_ID.getText());
            rs.next();
            student_firstName.setText(rs.getString("etunimi"));
            student_lastName.setText(rs.getString("sukunimi"));
            student_address.setText(rs.getString("lahiosoite"));
            student_postalNo.setText(rs.getString("postinro"));
            student_postalArea.setText(rs.getString("postitoimipaikka"));
            student_email.setText(rs.getString("email"));
            student_phoneNo.setText(rs.getString("puhelinnro"));
        } catch (SQLException e) {
            alertEvent(e);
        }
    }

    /**
     * Changes the values of a specific student
     */
    @FXML
    public void btnChangeStudent() {
        try {
            dbHandler.updateStudent(
                    student_ID.getText(),
                    student_firstName.getText(),
                    student_lastName.getText(),
                    student_address.getText(),
                    student_postalArea.getText(),
                    student_postalNo.getText(),
                    student_email.getText(),
                    student_phoneNo.getText());
        } catch (SQLException e) {
            alertEvent(e);
        }
    }

    /**
     * Removes a specific student
     * asks for confirmation from user
     */
    @FXML
    public void btnRemoveStudent() {
        try {
            ResultSet rs = dbHandler.getSpecificStudent(student_ID.getText());
            if (rs != null) {
                rs.next();
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setHeaderText("Poisto vahvistus");
                alert.setContentText("Haluatko poistaa opiskelijan: " +
                        rs.getInt("opiskelija_id") + " " +
                        rs.getString("etunimi") + " " +
                        rs.getString("sukunimi"));
                Optional<ButtonType> buttonResult = alert.showAndWait();
                if (buttonResult.isPresent()) {
                    if (buttonResult.get() == ButtonType.OK) {
                        dbHandler.removeStudent(student_ID.getText());
                    }
                }
            }
        } catch (SQLException e) {
            alertEvent(e);
        }
    }

    /**
     * Used for filling the student table
     * gets its data from the database
     * uses student objects to show it on the tableview
     */
    @FXML
    public void btnSearchStudent() {

        /*
        Clearing the table before filling it so it doesn't retain previous searches
         */
        student_table.getItems().clear();

        try {
            ResultSet rs = dbHandler.getStudentDetails(studentSearch.getText());
            while (rs.next()) {
                student_table.getItems().add(new student(
                        rs.getInt("opiskelija_id"),
                        rs.getString("etunimi"),
                        rs.getString("sukunimi"),
                        rs.getString("lahiosoite"),
                        rs.getString("postitoimipaikka"),
                        rs.getString("puhelinnro"),
                        rs.getString("email"),
                        rs.getString("postinro")));
            }
        } catch (SQLException e) {
            alertEvent(e);
        }
    }
}