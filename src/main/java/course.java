/**
 * Used to display info on the TableView
 */
public class course {

    private int course_ID;
    private String name;
    private int points;
    private String desc;

    /**
     * Constructs a course object used for TableView Cells
     *
     * @param course_ID integer gotten from the database
     * @param name      string gotten from the database
     * @param points    integer gotten from the database
     * @param desc      string can be quite long from the database
     */
    public course(int course_ID, String name, int points, String desc) {
        this.course_ID = course_ID;
        this.name = name;
        this.points = points;
        this.desc = desc;
    }

    public boolean equals(course c) {
        return this.course_ID == Integer.parseInt(c.getCourse_ID()) &&
                this.name.equals(c.getName()) &&
                this.points == Integer.parseInt(c.getPoints()) &&
                this.desc.equals(c.getDesc());
    }

    /**
     * Gets string value of course_ID to show on TableView student_ID column
     * @return string representation of course_ID
     */
    public String getCourse_ID() {
        return String.valueOf(course_ID);
    }

    /**
     * Required for TableView even though not called in code
     * @return name as a string
     */
    public String getName() {
        return name;
    }

    /**
     * Required for TableView not called in code
     * @return string representation of points
     */
    public String getPoints() {
        return String.valueOf(points);
    }

    /**
     * Required for TableView not called in code
     * @return a string that can be quite long
     */
    public String getDesc() {
        return desc;
    }
}
