import java.sql.Date;

/**
 * Used to display course completions in a TableView
 */
public class courseCompletion {

    private int student_ID;
    private int course_ID;
    private int grade;
    private Date completion_date;

    /**
     * Filled with info from the database, used for TableViews
     *
     * @param student_ID      integer identifier for student
     * @param course_ID       integer identifier for course
     * @param grade           int of grade usually 0-5
     * @param completion_date SQL Date from database
     */
    public courseCompletion(int student_ID, int course_ID, int grade, Date completion_date) {
        this.student_ID = student_ID;
        this.course_ID = course_ID;
        this.grade = grade;
        this.completion_date = completion_date;
    }

    /**
     * Required for TableView
     * @return string value of course_ID
     */
    public String getCourse_ID() {
        return String.valueOf(course_ID);
    }

    /**
     * Required for TableView
     * @return string value of grade
     */
    public String getGrade() {
        return String.valueOf(grade);
    }

    /**
     * Required for TableView
     * @return a string representation of date
     */
    public String getCompletion_date() {
        return completion_date.toString();
    }

    /**
     * Required for TableView
     * @return string value of student_ID
     */
    public String getStudent_ID() {
        return String.valueOf(student_ID);
    }

    public boolean equals(courseCompletion cc) {
        return (this.course_ID == Integer.parseInt(cc.getCourse_ID()) &&
                this.student_ID == Integer.parseInt(cc.getStudent_ID()) &&
                this.grade == Integer.parseInt(cc.getGrade()) &&
                this.completion_date.equals(Date.valueOf(cc.getCompletion_date())));
    }
}
