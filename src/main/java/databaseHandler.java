import java.sql.*;

/**
 * Interacts with the SQL database
 * queries should be prepared to prevent SQL injection
 */
public class databaseHandler {

    private String url = "jdbc:mariadb://localhost:3306/opsuor";

    private String user = "root";
    private String password = "root";
    private Connection connection;
    private PreparedStatement statement;

    /**
     * DB handler, used for connecting, modifying and getting data out of the database
     * connects to a default address with predetermined user and password
     */
    public databaseHandler() {
        try {
            sqlConnect();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Libraries were probably missing or there was a connection issue");
        }
    }

    /**
     * DB handler, which lets you select a URL, user and password
     *
     * @param url      jbdc address for example "jbdc:mariadb://localhost:3306/(database)"
     * @param user     username
     * @param password database password
     */
    public databaseHandler(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
        try {
            sqlConnect();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Libraries were probably missing or there was a connection issue");
        }
    }

    /**
     * Connect method that tries to connect to a database
     * @throws SQLException when there is an issue
     */
    public void sqlConnect() throws SQLException {
        this.connection = null;
        this.connection = DriverManager.getConnection(this.url, this.user, this.password);
    }

    /**
     * Disconnect method disconnects from the database
     * not used in the project, but maybe in the future
     * @throws SQLException when there is a issue with DB
     */
    public void sqlDisconnect() throws SQLException {
        if (this.connection != null) {
            this.connection.close();
        }
    }


    /**
     * Executes a query on the database to find course completions for a specific student
     *
     * @param student_ID takes in a student_ID in the form of numbers
     * @return ResultSet or null in the case of an error
     */
    public ResultSet getStudentSearch(String student_ID) throws SQLException {
        if (student_ID.equals("")) {
            String query = "SELECT * FROM opintosuoritus";
            statement = connection.prepareStatement(query);
            return statement.executeQuery();
        } else {
            String query = "SELECT * FROM opintosuoritus WHERE opiskelija_id = ?";
            statement = connection.prepareStatement(query);
            statement.setString(1, student_ID);
            return statement.executeQuery();
        }
    }

    /**
     * Executes a query on the database to find course completions for a specific student
     *
     * @param course_ID takes in a course_id in the form of numbers
     * @return ResultSet
     */
    public ResultSet getCourseSearch(String course_ID) throws SQLException {
        if (course_ID.equals("")) {
            String query = "SELECT * FROM opintosuoritus";
            statement = connection.prepareStatement(query);
            return statement.executeQuery();
        } else {
            String query = "SELECT * FROM opintosuoritus WHERE kurssi_id = ?";
            statement = connection.prepareStatement(query);
            statement.setString(1, course_ID);
            return statement.executeQuery();
        }
    }

    /**
     * Gets specific course details or all if course_id == ""
     *
     * @param course_ID max length 11
     * @return Result set of multiple entries to display courses
     */
    public ResultSet getCourseDetails(String course_ID) throws SQLException {
        if (course_ID.equals("")) {
            String query = "SELECT * FROM kurssi";
            statement = connection.prepareStatement(query);
            return statement.executeQuery();

        } else {
            String query = "SELECT * FROM kurssi WHERE kurssi_id = ?";
            statement = connection.prepareStatement(query);
            statement.setString(1, course_ID);
            return statement.executeQuery();
        }
    }

    /**
     * Gets specific student details or all if student_ID == "" from student table
     *
     * @param student_ID max length 11
     * @return Result set of multiple entries to display students
     */
    public ResultSet getStudentDetails(String student_ID) throws SQLException {

        if (student_ID.equals("")) {
            String query = "SELECT * FROM opiskelija"; //returns every entry if not specified
            statement = connection.prepareStatement(query);
            return statement.executeQuery();

        } else {
            String query = "SELECT * FROM opiskelija WHERE opiskelija_id = ?";
            statement = connection.prepareStatement(query);
            statement.setString(1, student_ID);
            return statement.executeQuery();
        }
    }

    /**
     * Gets specific student details from student table
     *
     * @param student_ID max length 11
     * @return One student excluding ID
     */
    public ResultSet getSpecificStudent(String student_ID) throws SQLException {

        String query = "SELECT * FROM opiskelija WHERE opiskelija_id = ?";
        statement = connection.prepareStatement(query);
        statement.setString(1, student_ID);
        return statement.executeQuery();
    }

    /**
     * Adds student to the student table
     *
     * @param student_ID  max length 11
     * @param firstName   max length 20
     * @param lastName    max length 40
     * @param address     max length 40
     * @param postalArea  max length 30
     * @param postNumber  max length 5
     * @param email       max length 50
     * @param phoneNumber max length 15
     * @throws SQLException when there is an issue adding the information to the database
     * for example the ID already exists (no duplicates allowed)
     */
    public void addStudent(String student_ID, String firstName, String lastName,
                           String address, String postalArea,
                           String postNumber, String email, String phoneNumber) throws SQLException {
        String query = "INSERT INTO opiskelija (opiskelija_id, etunimi, sukunimi, lahiosoite, postitoimipaikka, postinro, email, puhelinnro) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        statement = connection.prepareStatement(query);
        statement.setString(1, student_ID);
        statement.setString(2, firstName);
        statement.setString(3, lastName);
        statement.setString(4, address);
        statement.setString(5, postalArea);
        statement.setString(6, postNumber);
        statement.setString(7, email);
        statement.setString(8, phoneNumber);
        statement.executeUpdate();
    }

    /**
     * Modifies values of a given student_id in the opiskelija table
     *
     * @param student_ID  max length 11
     * @param firstName   max length 20
     * @param lastName    max length 40
     * @param address     max length 40
     * @param postalArea  max length 30
     * @param postNumber  max length 5
     * @param email       max length 50
     * @param phoneNumber max length 15
     * @throws SQLException if there was an issue changing the information
     */
    public void updateStudent(String student_ID, String firstName, String lastName,
                              String address, String postalArea,
                              String postNumber, String email, String phoneNumber) throws SQLException {

        String query = "UPDATE opiskelija SET etunimi = ?, sukunimi = ?, lahiosoite = ?, postitoimipaikka = ?, postinro = ?, email = ?, puhelinnro = ? WHERE opiskelija_id = ?";
        statement = connection.prepareStatement(query);
        statement.setString(1, firstName);
        statement.setString(2, lastName);
        statement.setString(3, address);
        statement.setString(4, postalArea);
        statement.setString(5, postNumber);
        statement.setString(6, email);
        statement.setString(7, phoneNumber);
        statement.setString(8, student_ID);
        statement.executeUpdate();
    }

    /**
     * Removes given student_id from student table
     *
     * @param student_ID max length 11
     * @throws SQLException if the given ID doesn't exists for example
     */
    public void removeStudent(String student_ID) throws SQLException {
        String query = "DELETE FROM opiskelija WHERE opiskelija_id = ?";
        statement = connection.prepareStatement(query);
        statement.setString(1, student_ID);
        statement.executeUpdate();
    }

    /**
     * Adds a course to the course table
     *
     * @param course_ID   course id should be less than 11 char length
     * @param courseName  name should be less than 40 char length
     * @param points      points should be less than 11 char length
     * @param description description should be less that 255 char length
     * @throws SQLException if for example duplicate or one of the fields too long
     */
    public void addCourse(String course_ID, String courseName, String points, String description) throws SQLException {

        String query = "INSERT INTO kurssi (kurssi_id, nimi, opintopisteet, kuvaus) VALUES (?, ?, ?, ?)";
        statement = connection.prepareStatement(query);
        statement.setString(1, course_ID);
        statement.setString(2, courseName);
        statement.setString(3, points);
        statement.setString(4, description);
        statement.executeUpdate();
    }

    /**
     * Modifies a course on the database with the given course_id in the course table
     *
     * @param course_ID   course id should be less than 11 char length
     * @param courseName  name should be less than 40 char length
     * @param points      points should be less than 11 char length
     * @param description description should be less that 255 char length
     * @throws SQLException if one of the required fields was missing for example or too long
     */
    public void updateCourse(String course_ID, String courseName, String points, String description) throws SQLException {

        String query = "UPDATE kurssi SET nimi = ?, opintopisteet = ?, kuvaus = ? WHERE kurssi_id = ?";
        statement = connection.prepareStatement(query);
        statement.setString(1, courseName);
        statement.setString(2, points);
        statement.setString(3, description);
        statement.setString(4, course_ID);
        statement.executeUpdate();
    }

    /**
     * Removes a course from the database in course table
     *
     * @param course_ID max length 11
     * @throws SQLException if the course_ID doesn't exist for example
     */
    public void removeCourse(String course_ID) throws SQLException {
        String query = "DELETE FROM kurssi WHERE kurssi_id = ?";
        statement = connection.prepareStatement(query);
        statement.setString(1, course_ID);
        statement.executeUpdate();
    }

    /**
     * Gets specific course details or all if course_id == ""
     *
     * @param course_ID max length 11
     * @return Result set for one course excluding ID
     */
    public ResultSet getSpecificCourse(String course_ID) throws SQLException {

        String query = "SELECT nimi, opintopisteet, kuvaus FROM kurssi WHERE kurssi_id = ?";
        statement = connection.prepareStatement(query);
        statement.setString(1, course_ID);
        return statement.executeQuery();
    }

    /**
     * Adds a course completion to the database to the courseCompletion table
     *
     * @param student_ID max length 11
     * @param course_ID  max length 11
     * @param grade      max length 11
     * @param date       date
     * @throws SQLException in case of duplicates for instance
     */
    public void addCompletion(String course_ID, String student_ID, String grade, String date) throws SQLException {

        String query = "INSERT INTO opintosuoritus (opiskelija_id, kurssi_id, arvosana, suoritus_pvm) VALUES (?, ?, ?, ?)";
        statement = connection.prepareStatement(query);
        statement.setString(1, student_ID);
        statement.setString(2, course_ID);
        statement.setString(3, grade);
        statement.setString(4, date);
        statement.executeUpdate();
    }

    /**
     * Updates a specific completion in completion table
     *
     * @param student_ID max length 11
     * @param course_ID  max length 11
     * @param grade      max length 11
     * @param date       date
     * @throws SQLException if for example one or more of the required fields were missing
     */
    public void updateCompletion(String course_ID, String student_ID, String grade, String date) throws SQLException {

        String query = "UPDATE opintosuoritus SET arvosana = ?, suoritus_pvm = ? WHERE kurssi_id = ? AND opiskelija_id = ?";
        statement = connection.prepareStatement(query);
        statement.setString(1, grade);
        statement.setString(2, date);
        statement.setString(3, course_ID);
        statement.setString(4, student_ID);
        statement.executeUpdate();
    }

    /**
     * Removes a specific completion entry from the completion table
     *
     * @param course_ID  max length 11
     * @param student_ID max length 11
     * @throws SQLException if the given ID doesn't exists for example
     */
    public void removeCompletion(String course_ID, String student_ID) throws SQLException {
        String query = "DELETE FROM opintosuoritus WHERE kurssi_id = ? AND opiskelija_id = ?";
        statement = connection.prepareStatement(query);
        statement.setString(1, course_ID);
        statement.setString(2, student_ID);
        statement.executeUpdate();
    }

    /**
     * Gets specific completion for a course and a student
     *
     * @param course_ID  max length 11
     * @param student_ID max length 11
     * @return Result set of one completion excluding IDs
     */
    public ResultSet getSpecificCompletion(String course_ID, String student_ID) throws SQLException {
        String query = "SELECT arvosana, suoritus_pvm FROM opintosuoritus WHERE kurssi_id = ? AND opiskelija_id = ?";
        statement = connection.prepareStatement(query);
        statement.setString(1, course_ID);
        statement.setString(2, student_ID);
        return statement.executeQuery();
    }
}