import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Used for running the program
 * restricts the user from changing window size
 * inconsistent behavior when resizable
 *
 * Requires mariadb-java-client to interface with the database
 *
 */
public class mainFile extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stg) throws Exception {

        //Loading FXML
        Parent root = FXMLLoader.load(getClass().getResource("Main.fxml"));

        //Defining window size
        Scene scn = new Scene(root, 1000, 700);
        stg.setScene(scn);
        stg.setTitle("SQL tietokannan hallintaohjelma");
        stg.show();
        stg.setResizable(false);
        stg.setMinHeight(600);
        stg.setMinWidth(900);
    }
}