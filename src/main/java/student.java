import java.sql.ResultSet;

/**
 * Used to display student data in TableView
 */

public class student {

    private int student_ID;
    private String firstName, lastName, address, postArea, postNum, email, phoneNum;

    /**
     * Used to display student information in TableView
     * filled using the database
     *
     * @param student_ID integer from db
     * @param firstName  string from db
     * @param lastName   -
     * @param address    -
     * @param postArea   -
     * @param postNum    -
     * @param email      -
     * @param phoneNum   -
     */
    public student(int student_ID, String firstName, String lastName, String address, String postArea, String phoneNum, String email, String postNum) {

        this.student_ID = student_ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.postArea = postArea;
        this.postNum = postNum;
        this.email = email;
        this.phoneNum = phoneNum;
    }

    /**
     * Required for TableView to function
     * @return a string representation of student_ID
     */
    public String getStudent_ID() {
        return String.valueOf(student_ID);
    }

    /**
     * Required for TableView to function
     * @return first name of the student as a string
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Required for TableView to function
     * @return last name of the student as a string
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Required for TableView to function
     * @return address as string
     */
    public String getAddress() {
        return address;
    }

    /**
     * Required for TableView to function
     * @return postal area as string
     */
    public String getPostArea() {
        return postArea;
    }

    /**
     * Required for TableView to function
     * @return post number as string
     */
    public String getPostNum() {
        return postNum;
    }

    /**
     * Required for TableView to function
     * @return email address as a string
     */
    public String getEmail() {
        return email;
    }

    /**
     * Required for TableView to function
     * @return phone number
     */
    public String getPhoneNum() {
        return phoneNum;
    }

    public String toString() {
        return student_ID + ":" + firstName + ":" + lastName + ":" + address + ":" + postArea + ":" + postNum + ":" + email + ":" + phoneNum;
    }
    public boolean equals(student s) {
        return this.student_ID == Integer.parseInt(s.getStudent_ID()) &&
                this.firstName.equals(s.getFirstName()) &&
                this.lastName.equals(s.getLastName()) &&
                this.address.equals(s.getAddress()) &&
                this.postArea.equals(s.getPostArea()) &&
                this.postNum.equals(s.getPostNum()) &&
                this.email.equals(s.getEmail()) &&
                this.phoneNum.equals(s.getPhoneNum());
    }
}
