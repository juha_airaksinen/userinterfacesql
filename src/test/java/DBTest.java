import org.junit.Test;
import org.junit.internal.runners.statements.Fail;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class DBTest {

    student eS = new student(5, "FirstName", "LastName", "Address", "postArea", "phoneNum", "email", "postN");
    course eC = new course(5, "CourseName", 5, "Description");
    courseCompletion ecC =
                    new courseCompletion(5, 5, 5, Date.valueOf("2020-05-05"));

    databaseHandler dbH = new databaseHandler();

    @Test
    public void testStudent() {
        try {
            dbH.removeStudent("5");
            dbH.addStudent(eS.getStudent_ID(), eS.getFirstName(), eS.getLastName(), eS.getAddress(), eS.getPostArea(), eS.getPostNum(), eS.getEmail(), eS.getPhoneNum());
        } catch (SQLException e) {
            fail(e.getMessage());
        }

        try {
            ResultSet rs = dbH.getSpecificStudent("5");
            rs.next();
            student tempStudent = new student(
                    rs.getInt("opiskelija_id"),
                    rs.getString("etunimi"),
                    rs.getString("sukunimi"),
                    rs.getString("lahiosoite"),
                    rs.getString("postitoimipaikka"),
                    rs.getString("puhelinnro"),
                    rs.getString("email"),
                    rs.getString("postinro"));

            assertTrue("Didn't equal", eS.equals(tempStudent));
        } catch (SQLException e) {
            fail(e.getMessage());
        }

        try {
            dbH.removeStudent("5");
        } catch (SQLException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testCourse() {
        try {
            dbH.removeCourse("5");
            dbH.addCourse(eC.getCourse_ID(), eC.getName(), eC.getPoints(), eC.getDesc());
        } catch (SQLException e) {
            fail(e.getMessage());
        }

        try {
            ResultSet rs = dbH.getSpecificCourse("5");
            rs.next();
            course tempCourse = new course(Integer.parseInt(eC.getCourse_ID()), eC.getName(), Integer.parseInt(eC.getPoints()), eC.getDesc());

            assertTrue("Didn't equal", eC.equals(tempCourse));
        } catch (SQLException e) {
            fail(e.getMessage());
        }

        try {
            dbH.removeCourse("5");
        } catch (SQLException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testCourseCompletion() {
        try {
            dbH.removeCourse("5");
            dbH.removeStudent("5");
            dbH.removeCompletion("5", "5");
            dbH.addCourse(eC.getCourse_ID(), eC.getName(), eC.getPoints(), eC.getDesc());
            dbH.addStudent(eS.getStudent_ID(), eS.getFirstName(), eS.getLastName(), eS.getAddress(), eS.getPostArea(), eS.getPostNum(), eS.getEmail(), eS.getPhoneNum());
            dbH.addCompletion(ecC.getCourse_ID(), ecC.getStudent_ID(), ecC.getGrade(), ecC.getCompletion_date());
        } catch (SQLException e) {
            fail(e.getMessage());
        }

        try {
            ResultSet rs = dbH.getSpecificCompletion("5", "5");
            rs.next();
            courseCompletion tempCourseC = new courseCompletion(5, 5, 5, Date.valueOf("2020-05-05"));

            assertTrue("Didn't equal", ecC.equals(tempCourseC));
        } catch (SQLException e) {
            fail(e.getMessage());
        }

        try {
            dbH.removeCourse("5");
            dbH.removeStudent("5");
            dbH.removeCompletion("5", "5");
        } catch (SQLException e) {
            fail(e.getMessage());
        }
    }
}
